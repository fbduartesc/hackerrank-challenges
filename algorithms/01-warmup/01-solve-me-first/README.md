## Solve me first

**HackerRank Link**: https://www.hackerrank.com/challenges/solve-me-first

**Difficulty**: Easy
 
**Solution(s) in**: JavaScript
###############################

Complete the function solveMeFirst to compute the sum of two integers.

**Function prototype:**

int solveMeFirst(int a, int b);

where,

* a is the first integer input.
* b is the second integer input

**Return values**

sum of the above two integers

**Sample Input**

```javascript
a = 2
b = 3
```

**Sample Output**

```javascript
5
```

**Explanation**

The sum of the two integers *a* and *b* is computed as: **2 + 3 = 5**. 