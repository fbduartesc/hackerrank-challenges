/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/solve-me-first)
 */

// Running main function for output
main();

// Solution Starts
// ==============

function solveMeFirst(a, b) {
    return a + b;
}

// Main function
function main(){
    let func = solveMeFirst(10,20);
    console.log(func);
}