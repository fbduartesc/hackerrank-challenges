/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/plus-minus)
 */

// Running main function for output
main();

// Solution Starts
// ==============

/*
 * Complete the plusMinus function below.
 *
 */
function plusMinus(arr) {
    let countPositive = 0,
        countNegative = 0,
        countZero = 0;
    for(let index = 0; index < arr.length; index++){
        if(Math.sign(arr[index]) === 1){
            ++countPositive;
        }else if(Math.sign(arr[index]) === -1){
            ++countNegative;
        }else{
            ++countZero;
        }
    }
    let iterable = new Map([['positive', countPositive], ['negative', countNegative], ['zero', countZero]]);
    for(let [key, value] of iterable){
        console.log((value/arr.length).toFixed(6));
    }
}

// Main function
function main() {
    plusMinus([-4, 3, -9, 0, 4, 1]);
}