/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/staircase)
 */

// Running main function for output
main();

// Solution Starts
// ==============

// Complete the staircase function below.
function staircase(size) {
    for(let index = 0; index < size; index++){
        let output = '';
        for(let indexColumn = 0; indexColumn < size; indexColumn++){
            indexColumn < (size-1-index) ? output += ' ' : output += '#';
        }
        console.log(output);
    }
}

// Main function
function main() {
    staircase(6);
}