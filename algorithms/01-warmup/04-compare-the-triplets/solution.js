/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/compare-the-triplets)
 */

// Running main function for output
main();

// Solution Starts
// ==============

function compareTriplets(a, b) {
    let score = [0,0];
    for (let indexAlice in a) {
        if(a[indexAlice] > b[indexAlice]){
            ++score[0];
        }else if(a[indexAlice] < b[indexAlice]){
            ++score[1];
        }
    }
    return score;
}

// Main function
function main(){
    let func = compareTriplets([5,6,7],[3,6,10]);
    console.log(func);
}