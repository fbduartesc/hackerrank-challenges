/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/time-conversion)
 */

// Running main function for output
main();

// Solution Starts
// ==============

// Complete the timeConversion function below.
function timeConversion(s) {
    const time = s.match(/(\d+):(\d+):(\d+)(..)/);
    let newTime = {
        hour: Number(time[1]),
        minute: Number(time[2]),
        second: Number(time[3]),
        format: time[4]
    };
    if ((newTime.format === 'PM') && newTime.hour < 12) {
        newTime.hour += 12;
    } else if ((newTime.format === 'AM') && (newTime.hour === 12)) {
        newTime.hour -= 12;
    }
    if (newTime.hour < 10) newTime.hour = newTime.hour.toString().padStart(2, '0');
    if (newTime.minute < 10) newTime.minute = newTime.minute.toString().padStart(2, '0');
    if (newTime.second < 10) newTime.second = newTime.second.toString().padStart(2, '0');

    // Output the new formatted time.
    return `${newTime.hour}:${newTime.minute}:${newTime.second}`;
}

// Main function
function main() {
    let result = timeConversion('12:45:54PM');
    console.log(result);
}