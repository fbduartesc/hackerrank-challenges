/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/simple-array-sum)
 */

// Running main function for output
main();

// Solution Starts
// ==============

function simpleArraySum(ar) {
    let sum = 0;
    for (let index = 0; index < ar.length; index++) {
        sum += ar[index];
    }
    return sum;
}

// Main function
function main(){
    let func = simpleArraySum([1,2,3,4,10,11]);
    console.log(func);
}