/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/birthday-cake-candles)
 */

// Running main function for output
main();

// Solution Starts
// ==============

// Complete the birthdayCakeCandles function below.
function birthdayCakeCandles(arr) {
    const max = Math.max(...arr);
    let countCandles = 0;
    arr.forEach(function (value) {
        (value === max) ? ++countCandles : '';
    });
    return countCandles;
}

// Main function
function main() {
    let result = birthdayCakeCandles([3,2,1,3]);
    console.log(result);
}