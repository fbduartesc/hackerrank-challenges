/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/a-very-big-sum)
 */

// Running main function for output
main();

// Solution Starts
// ==============

function aVeryBigSum(ar) {
    return ar.reduce((a, b)=>a+b);
}

// Main function
function main(){
    const ar = [5,1000000001,1000000002,1000000003,1000000004,1000000005];
    let func = aVeryBigSum(ar);
    console.log(func);
}