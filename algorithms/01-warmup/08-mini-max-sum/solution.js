/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/mini-max-sum)
 */

// Running main function for output
main();

// Solution Starts
// ==============

// Complete the miniMaxSum function below.
function miniMaxSum(arr) {
    let sum = 0;
    const max = Math.max(...arr), min = Math.min(...arr);
    arr.forEach(function(value){
        sum += value;
    });
    console.log(`${sum-max} ${sum-min}`);
}

// Main function
function main() {
    miniMaxSum([1,3,5,7,9]);
}