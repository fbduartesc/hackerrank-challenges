/*
HackerRank Adventures (https://gitlab.com/fbduartesc/hackerrank-challenges)
Author - Fabio Duarte de Souza (https://gitlab.com/fbduartesc)
Original challenge (https://www.hackerrank.com/challenges/diagonal-difference)
 */

// Running main function for output
main();

// Solution Starts
// ==============

/*
 * Complete the 'diagonalDifference' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */
function diagonalDifference(arr) {
    let firstSum = 0,
        secondSum = 0;

    for (let indexArrayAuxiliar = 0; indexArrayAuxiliar < arr.length; indexArrayAuxiliar++) {
        //Calculate primary decimal
        firstSum += arr[indexArrayAuxiliar][indexArrayAuxiliar];
        //Calculate second decimal
        secondSum += arr[indexArrayAuxiliar][Math.abs(indexArrayAuxiliar - (arr.length - 1))];
    }
    return Math.abs(firstSum - secondSum);
}

// Main function
function main() {
    const result = diagonalDifference([[11,2,4], [4,5,6], [10,8,-12]]);
    console.log(result);
}